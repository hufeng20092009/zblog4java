package com.javaear.zblog4java.system.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import org.springframework.util.Assert;

/**
 * @author aooer
 */
@TableName("template")
public final class TemplateModel extends IBaseModel {

    @TableField("user_id")
    private Integer userId;
    private String title;
    private String content;
    private String alias;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public void check() {
        Assert.notNull(userId);
        Assert.notNull(title);
        Assert.notNull(content);
    }
}
