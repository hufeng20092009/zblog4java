package com.javaear.zblog4java.system.controller;

import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;
import com.javaear.generalmapper.GeneralMapper;
import com.javaear.zblog4java.system.service.impl.AdminService;
import com.javaear.zblog4java.system.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author aooer
 */
@Controller
@RequestMapping("system")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private GeneralMapper generalMapper;

    @RequestMapping("index")
    public void index(ModelMap modelMap) {
        int articleCount=generalMapper.selectCount(new ArticleModel());
        modelMap.put("articleCount",articleCount);
        int cateCount=generalMapper.selectCount(new CategoryModel());
        modelMap.put("cateCount",cateCount);
        int templateCount=generalMapper.selectCount(new TemplateModel());
        modelMap.put("templateCount",templateCount);
        int tagCount=generalMapper.selectCount(new TagModel());
        modelMap.put("tagCount",tagCount);
        int commentCount=generalMapper.selectCount(new CommentModel());
        modelMap.put("commentCount",commentCount);
        int userCount=generalMapper.selectCount(new UserModel());
        modelMap.put("userCount",userCount);
    }

    @Login(action = Action.Skip)
    @RequestMapping("login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        return adminService.login(request, response);
    }

    @Login(action = Action.Skip)
    @RequestMapping("logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        return adminService.logout(request, response);
    }

    @Login(action = Action.Skip)
    @RequestMapping(value = "unpermission", method = RequestMethod.GET)
    public void unpermission(ModelMap modelMap) {
    }
}
