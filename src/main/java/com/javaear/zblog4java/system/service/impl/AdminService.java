package com.javaear.zblog4java.system.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.kisso.SSOConfig;
import com.baomidou.kisso.SSOHelper;
import com.baomidou.kisso.SSOToken;
import com.javaear.generalmapper.GeneralMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.javaear.zblog4java.system.entity.UserModel;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

/**
 * @author aooer
 */
@Service
public class AdminService {

    @Autowired
    private GeneralMapper userMapper;

    public String login(HttpServletRequest request, HttpServletResponse response) {
        if (SSOHelper.getToken(request) != null)
            return "redirect:index.html";
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String savedate = request.getParameter("savedate");
        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
            UserModel userModel = userMapper.selectOne(new UserModel(username));
            if (userModel != null && userModel.getPassword().equals(password)) {
                //authSSOCookie 设置 cookie 同时改变 jsessionId
                SSOToken ssoToken = new SSOToken(request);
                ssoToken.setData(JSON.toJSONString(userModel));
                //记住密码，设置 cookie 时长 1 周 = 604800 秒 【动态设置 maxAge 实现记住密码功能】
                if (savedate.equals("1"))
                    request.setAttribute(SSOConfig.SSO_COOKIE_MAXAGE, TimeUnit.DAYS.toSeconds(7));
                SSOHelper.setSSOCookie(request, response, ssoToken, true);
                return "redirect:index.html";
            }
        }
        return "system/login";
    }

    public String logout(HttpServletRequest request, HttpServletResponse response) {
        SSOHelper.clearLogin(request, response);
        return "redirect:login.html";
    }
}
