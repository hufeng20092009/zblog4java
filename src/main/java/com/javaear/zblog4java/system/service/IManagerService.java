package com.javaear.zblog4java.system.service;

/**
 * 通用模块管理服务
 *
 * @author aooer
 */
public interface IManagerService {

    /**
     * 模块查询
     */
    void queryAll();

    /**
     * 模块添加
     *
     * @return 返回url
     */
    String add();

    /**
     * 模块编辑
     *
     * @return 返回url
     */
    String edit();

    /**
     * 模块删除
     *
     * @return 返回url
     */
    String remove();
}
