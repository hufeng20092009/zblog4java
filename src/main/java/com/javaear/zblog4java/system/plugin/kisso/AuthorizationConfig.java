package com.javaear.zblog4java.system.plugin.kisso;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.javaear.generalmapper.GeneralMapper;
import com.javaear.zblog4java.system.entity.PermissionModel;
import com.javaear.zblog4java.system.entity.RoleModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author aooer
 */
@Component
public class AuthorizationConfig {

    @Autowired
    private GeneralMapper permissionMapper;

    @Cacheable("service")
    public List<PermissionModel> getPermissionModels() {
        return permissionMapper.selectList(new EntityWrapper<>(new PermissionModel()));
    }

    @Cacheable("service")
    public List<RoleModel> getRoleModels() {
        return permissionMapper.selectList(new EntityWrapper<>(new RoleModel()));
    }
}
