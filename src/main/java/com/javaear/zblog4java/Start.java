package com.javaear.zblog4java;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * @author aooer
 */
public class Start {

    public static void main(String[] args) throws Exception {
        WebAppContext context = new WebAppContext() {{
            setContextPath("");
            setResourceBase(System.getProperty("user.dir") + "\\src\\main\\webapp");
            setParentLoaderPriority(true);
        }};
        Server server = new Server(8080);
        server.setHandler(context);
        server.setStopAtShutdown(true);
        server.start();
        server.join();
    }

}
