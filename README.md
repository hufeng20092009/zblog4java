#zblog4java

特点：
无mapper.xml，无mapper接口，轻量级50k后台代码，看完源码只需要30分钟。

使用：
使用之前需要根据zblog4java.sql导入数据库表，并配置数据库链接信息classpath:config/datasource-config.xml替换实际的数据库连接信息

使用tomcat或点击内置的jetty容器start.java运行main函数启动项目后，访问地址http://localhost:8080/system/index.html即可访问

默认用户名密码：admin123 / administrator

示例图：
![输入图片说明](http://git.oschina.net/uploads/images/2016/1004/161134_e235c8b4_467423.png "在这里输入图片标题")

待问题：
前端js，IE不兼容，目前使用firefox访问
